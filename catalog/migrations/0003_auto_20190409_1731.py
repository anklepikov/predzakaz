# Generated by Django 2.1.7 on 2019-04-09 14:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0002_dateandplace'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cart',
            name='item_total',
            field=models.PositiveIntegerField(default=0, verbose_name='Общая сумма'),
        ),
    ]
