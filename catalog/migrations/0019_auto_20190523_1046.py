# Generated by Django 2.1.7 on 2019-05-23 07:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0018_auto_20190522_1451'),
    ]

    operations = [
        migrations.AddField(
            model_name='allvenues',
            name='href',
            field=models.CharField(blank=True, default='', max_length=100, verbose_name='Короткое название для ссылки'),
        ),
        migrations.AlterField(
            model_name='team',
            name='venue',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='catalog.CurrentVenue', verbose_name='Участвует в событии'),
        ),
    ]
