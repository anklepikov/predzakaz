$(document).ready(function(){
    $('.delete-elem').submit(function(e){
        e.preventDefault();

         $.ajax({
            method : "POST" ,
            url : $(this).attr('action'),
            data: {
                'goodId' : $(this).attr('id'),
            },

            dataType: 'json',
            success: function(data){
              $(".cart-table").find("."+data.con[0][0]).css("display", "none");
              $(".ItemTotalSum").text(data.con[0][1]);
//                $(".cart-table tr").remove();
//
//
//                for (var i=0;i<data.lenSub;i+=1){
//                    var table_row='<tr><td><img class="cart-item-img" src="' +data.con[i][2]+ '"alt="' +data.con[i][1]+ '"></td>';
//                    table_row+='<td class="align-middle">'+data.con[i][1]+'</td><td class="align-middle"><button class="cart-good-minus btn btn-light">-</button>';
//                    table_row+='<span>' +data.con[i][3]+'</span><button class="cart-good-plus btn btn-light">+</button>';
//                    table_row+='<span>шт. по '+data.con[i][4] +' р.</span></td><td class="align-middle">';
//                    table_row+='<form class="delete-elem" id="'+data.con[i][0]+'" action="/'+data.con[i][5]+'/cart/delete" method="post">';
//                    table_row+='<button class="cart-good-delete  btn btn-light" type="submit">Удалить</button></form></td></tr>';
//                     $('.cart-table').append(table_row);
//                }
            }
        })
    });

$('.plus-elem').submit(function(e){
        e.preventDefault();
        aid = $(this).attr('id').split("-")[1];

         $.ajax({
            method : "POST" ,
            url : $(this).attr('action'),
            data: {
                'goodId' : aid,
            },

            dataType: 'json',
            success: function(data){

              $(".cart-table").find("."+data.con[0][0]).find(".quantity").text(data.con[0][1]);
              $(".ItemTotalSum").text(data.con[0][2]);

            }
        })
    });
$('.minus-elem').submit(function(e){
        e.preventDefault();
        aid = $(this).attr('id').split("-")[1];
        quantity = $(this).siblings(".quantity").eq(0).text();
        if (quantity > 1){

           $.ajax({
            method : "POST" ,
            url : $(this).attr('action'),
            data: {
                'goodId' : aid,
            },

            dataType: 'json',
            success: function(data){

              $(".cart-table").find("."+data.con[0][0]).find(".quantity").text(data.con[0][1]);
              $(".ItemTotalSum").text(data.con[0][2]);

            }
        })
        }

    });



   function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
};
var csrftoken = getCookie('csrftoken');
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
};
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

});

