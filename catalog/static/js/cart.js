$(document).ready(function(){
    $('.delete-elem').submit(function(e){
        e.preventDefault();
            all = $(this).attr('id').split("-");
         $.ajax({
            method : "POST" ,
            url : $(this).attr('action'),
            data: {
                'goodId' : all[0],
                'cookieId' : all[1],
                'uniqueness' : all[2],
            },

            dataType: 'json',
            success: function(data){
              $(".cart-table").find("."+data.con[0][0]).css("display", "none");
              $(".ItemTotalSum").text(data.con[0][1]);
             a = Number($("span."+all[1]+"-"+all[2])[0].innerText);
              a -= Number(data.con[0][2]);
              $("span."+all[1]+"-"+all[2])[0].innerText=a;
              if (0 == a){
                $("span."+all[1]+"-"+all[2]).parent().css("display", "none");
                $("#nameCookie"+all[1]).css("display", "none");
              }
            }
        })
    });

$('.plus-elem').submit(function(e){
        e.preventDefault();
        aid = $(this).attr('id').split("-");

         $.ajax({
            method : "POST" ,
            url : $(this).attr('action'),
            data: {
                'goodId' : aid[1],
                'cookieId' : aid[2],
                'uniqueness' : aid[3],
            },

            dataType: 'json',
            success: function(data){

              $(".cart-table").find("."+data.con[0][0]).find(".quantity").text(data.con[0][1]);
              $(".ItemTotalSum").text(data.con[0][2]);
              a = Number($("span."+aid[2]+"-"+aid[3])[0].innerText);
              a += Number(data.con[0][3]);
            $("span."+aid[2]+"-"+aid[3])[0].innerText=a;
              }
        })
    });
$('.minus-elem').submit(function(e){
        e.preventDefault();
        aid = $(this).attr('id').split("-");
        quantity = $(this).siblings(".quantity").eq(0).text();
        if (quantity > 1){

           $.ajax({
            method : "POST" ,
            url : $(this).attr('action'),
            data: {
                'goodId' : aid[1],
                'cookieId' : aid[2],
                'uniqueness' : aid[3],
            },

            dataType: 'json',
            success: function(data){

              $(".cart-table").find("."+data.con[0][0]).find(".quantity").text(data.con[0][1]);
              $(".ItemTotalSum").text(data.con[0][2]);
               a = Number($("span."+aid[2]+"-"+aid[3])[0].innerText);
              a -= Number(data.con[0][3]);
              $("span."+aid[2]+"-"+aid[3])[0].innerText=a;
            }
        })
        }

    });
$('.add-good').submit(function(e){
    e.preventDefault();
    var id = $(this).children()[1].getAttribute('id');
     $.ajax({
        method : "POST" ,
        url : $(this).attr('action'),
        data: {
            'good' : id,
        },

        dataType: 'json',
        success: function(data){
           if (data.some == "N"){
                $('#nameModal').modal({
                    show: 'true'
                });
           }
           else{
              $(document).find(".NumberGoods").text(data.some);
              $("#" + id)
                .clone()
                .css({'position' : 'absolute', 'z-index' : '11100', top: $("#" + id).offset().top, left:$("#" + id).offset().left})
                .appendTo("body")
                .animate({opacity: 0.05,
                    left: $("#iconcart").offset()['left'],
                    top: $("#iconcart").offset()['top'],
                    width: 20,height: 20}, 1000, function() {
                    $(this).remove();
                });

           }
        }
     })

});

$('.cookiesave').on("submit", function(e){
    e.preventDefault();
     $.ajax({
        method : "POST" ,
        url : $(this).attr('action'),
        data: {
            'nameUser' : $('.nameValue')[0].value,
            'nameTeam' : $(this).attr('id'),
        },

        dataType: 'json',
        success: function(data){

            if (data.con[0][0] == "m"){

                $('#uniqueModal').modal({
                    show: true
                });
            }
            else{
                $(".cookieUserName").text(data.con[0][1]);
            }
        }
     })
});

$('.addarchive').submit(function(e){
    e.preventDefault();
    str = $(this).attr('id').split("-");
     $.ajax({
        method : "POST" ,
        url : $(this).attr('action'),
        data: {
            'team' : str[0],
            'cookiename' : str[1],
            'cookieuniq' : str[2],
        },

        dataType: 'json',
        success: function(data){
                $('#nameModal').modal({
                    show: 'false'
                });
                $(document).find(".NumberGoods").text(data.some);
        }
     })

});




$(".saveButton").on('click', function() {
        $(".cookiesave").submit();
        $(".addarchive").css("display", "none");
});


$(".addButton").on('click', function() {
        $(".addarchive").submit();
});


$("body").on("click" ,".unconnectButton", function(){
    $('#nameModal').modal({
        show: 'true'
    });

});

$("body").on("click" ,".connectButton", function(){
    $.ajax({
        method : "POST" ,
        url : $(this).parent().parent().attr('action'),
        data: {
            'NameConnectUser' : $(this).parent().parent().attr('action').split("/")[2],
            'cookieName': $('.nameValue')[0].value,
        },

        dataType: 'json',
        success: function(data){

        }
     })

     $(".cookieUserName").text($('.nameValue')[0].value);
});

   function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
};
var csrftoken = getCookie('csrftoken');
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
};
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

});

