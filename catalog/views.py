from django.shortcuts import render
from .models import *
from django.contrib.auth import logout
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseRedirect , HttpResponse , JsonResponse
from django.db.models import Sum
@csrf_exempt
def catalog(request, token):
    if (token == "cookiesave"):
        cookie = str(request.POST['nameUser'])
        cookie = cookie.strip()
        cookie = cookie.replace(' ', '_')
        cookie = cookie.replace(',', '')
        cookie = cookie.replace('.', '')
        cookie = cookie.replace(';', '')
        anotherOrders = Carts.objects.filter(team_id = int(request.POST['nameTeam']), cookie_id = cookie, place = Team.objects.get(id = int(request.POST['nameTeam'])).venue.place)
        if (len(anotherOrders) > 0):
            con = dict()
            sum = 0
            for order in anotherOrders:
               sum += order.item_total
            con.update({0: ["m", cookie]})
            con.update({1: [1, sum]})
            return JsonResponse(fullDict(con))
        else:
            con = dict()
            con.update({0: ["o", cookie]})
            request.session['thisUser'] = cookie
            request.session['toConnect'] = str("1")
            return JsonResponse(fullDict(con))
    elif (token == "addarchive"):
        thisTeam = Team.objects.get(id=request.POST['team'])
        archiveCart = Archive.objects.filter(place = thisTeam.venue.place, team_id=request.POST['team'], cookie_id=request.POST['cookiename'],uniqueness=request.POST['cookieuniq'])
        for cart in archiveCart:
            try:
                existObj = Carts.objects.get(place = thisTeam.venue.place, cookie_id=cart.cookie_id, team_id=cart.team_id, uniqueness=cart.uniqueness, good = cart.good)
                existObj.quantity += cart.quantity
                existObj.item_total += cart.item_total
                existObj.save()
                thisTeam.cart_total += cart.item_total
            except:
                ibj = Carts.objects.create(good=cart.good, place = thisTeam.venue.place, quantity=cart.quantity, item_total=cart.item_total,team_id=cart.team_id, cookie_id=cart.cookie_id, uniqueness=cart.uniqueness)
                thisTeam.cart_total += cart.item_total
                ibj.save()
        archiveCart.delete()
        thisTeam.save()

        countgoods = 0
        for cart in Carts.objects.filter(place = thisTeam.venue.place, team_id=request.POST['team']):
            countgoods += cart.quantity

        return JsonResponse(toDict(countgoods))
    else:
        context = {}
        thisToken = str(token)
        thisTeam = Team.objects.get(token=thisToken)
        context['team'] = thisTeam
        context["is_play"] = False if thisTeam.venue == None else True
        if 'thisUser' in request.session:
            context['cookieUserName'] = request.session['thisUser']
            context['cookieUniqueness'] = request.session['toConnect']

            try:

                context['PreviousOrder'] = Archive.objects.filter(team_id = thisTeam.id, place = thisTeam.venue.place, cookie_id = request.session['thisUser'], uniqueness = request.session['toConnect'])
                context['itemTotal'] = context['PreviousOrder'].aggregate(Sum('item_total'))
            except:
                pass
        if (thisTeam.venue != None):
            thisCat = Categories.objects.filter(place = thisTeam.venue.place)
            for this in thisCat:
                if (False == this.has_subcategories):
                    this.onesubcategory = Subcategories.objects.get(category_name=this)
            context['categories'] = thisCat

            countgoods = 0
            for cart in Carts.objects.filter(team_id=thisTeam.id, place = thisTeam.venue.place):
                countgoods += cart.quantity
            context['countGoods'] = countgoods
            context['dateAndPlace'] = CurrentVenue.objects.get(place = thisTeam.venue.place)
        context['team'] = thisTeam
        context['registry'] = Registry.objects.last()
        return render(request, 'catalog/catalog.html', context)

def subcategory(request, token, cat):
    if (token == "addarchive"):
        thisTeam = Team.objects.get(id=request.POST['team'])
        archiveCart = Archive.objects.filter(team_id=request.POST['team'], place = thisTeam.venue.place, cookie_id=request.POST['cookiename'],uniqueness=request.POST['cookieuniq'])

        for cart in archiveCart:
            try:
                existObj = Carts.objects.get(cookie_id = cart.cookie_id, place = thisTeam.venue.place, team_id = cart.team_id,uniqueness = cart.uniqueness, good = cart.good)
                existObj.quantity += cart.quantity
                existObj.item_total += cart.item_total
                existObj.save()
                thisTeam.cart_total += cart.item_total
            except:
                ibj = Carts.objects.create(good=cart.good, quantity=cart.quantity, item_total=cart.item_total, place = thisTeam.venue.place, team_id=cart.team_id, cookie_id=cart.cookie_id, uniqueness=cart.uniqueness)
                thisTeam.cart_total += cart.item_total
                ibj.save()
        archiveCart.delete()
        thisTeam.save()

        countgoods = 0
        for cart in Carts.objects.filter(place = thisTeam.venue.place, team_id=request.POST['team']):
            countgoods += cart.quantity

        return JsonResponse(toDict(countgoods))
    else:
        context = {}
        sub = str(cat)
        if (sub == "cart"):
            if 'thisUser' in request.session:
                UserName = request.session['thisUser']
                thisToken = str(token)
                thisTeam = Team.objects.get(token=thisToken)
                context["is_play"] = False if thisTeam.venue == None else True
                if (thisTeam.venue != None):
                    carts = Carts.objects.filter(team_id=thisTeam.id, place = thisTeam.venue.place).order_by("cookie_id", "uniqueness")
                    if ("false" == request.session['toConnect']):
                        UserUniqueness = -1
                    else:
                        UserUniqueness = int(request.session['toConnect'])
                    listOfCarts = []
                    I = 0
                    if (len(carts) > 0):
                        word = carts[0].cookie_id
                        numUniq = carts[0].uniqueness

                    total = 0
                    listOneRecord = []
                    for cart in carts:
                        cart.sum = 0
                        if (word == cart.cookie_id):
                            if (numUniq == cart.uniqueness):
                                listOneRecord.append(cart)
                                total += cart.item_total
                            else:
                                listOneRecord[-1].sum = total
                                oldword = word
                                oldnumUniq = numUniq
                                total = cart.item_total
                                word = cart.cookie_id
                                numUniq = cart.uniqueness
                                if (UserName == oldword and UserUniqueness == oldnumUniq):
                                    listOfCarts.insert(0, listOneRecord)

                                else:
                                    listOfCarts.append(listOneRecord)

                                listOneRecord = [cart]

                        else:
                            listOneRecord[-1].sum = total
                            oldword = word
                            oldnumUniq = numUniq
                            total = cart.item_total
                            word = cart.cookie_id
                            numUniq = cart.uniqueness

                            if (UserName == oldword and UserUniqueness == oldnumUniq):
                                listOfCarts.insert(0, listOneRecord)
                            else:
                                listOfCarts.append(listOneRecord)
                            listOneRecord = [cart]
                        I += 1
                        if (len(carts) == I):
                            listOneRecord[-1].sum = total
                            if (UserName == word and UserUniqueness == numUniq):
                                listOfCarts.insert(0, listOneRecord)
                            else:
                                listOfCarts.append(listOneRecord)

                    context['team'] = thisTeam

                    context['carts'] = listOfCarts
                context['registry'] = Registry.objects.last()
                return render(request, 'catalog/cart.html', context)
            else:
                thisToken = str(token)
                thisTeam = Team.objects.get(token=thisToken)
                context["is_play"] = False if thisTeam.venue == None else True
                if (thisTeam.venue != None):
                    carts = Carts.objects.filter(team_id=thisTeam.id, place = thisTeam.venue.place).order_by("cookie_id", "uniqueness")
                    listOfCarts = []
                    I = 0
                    if (len(carts) > 0):
                        word = carts[0].cookie_id
                        numUniq = carts[0].uniqueness
                    total = 0
                    listOneRecord = []
                    for cart in carts:
                        cart.sum = 0
                        if (word == cart.cookie_id):
                            if (numUniq == cart.uniqueness):
                                listOneRecord.append(cart)
                                total += cart.item_total
                            else:
                                listOneRecord[-1].sum = total
                                oldword = word
                                oldnumUniq = numUniq
                                total = cart.item_total
                                word = cart.cookie_id
                                numUniq = cart.uniqueness

                                listOfCarts.append(listOneRecord)

                                listOneRecord = [cart]

                        else:
                            listOneRecord[-1].sum = total
                            oldword = word
                            oldnumUniq = numUniq
                            total = cart.item_total
                            word = cart.cookie_id
                            numUniq = cart.uniqueness


                            listOfCarts.append(listOneRecord)
                            listOneRecord = [cart]
                        I += 1
                        if (len(carts) == I):
                            listOneRecord[-1].sum = total

                            listOfCarts.append(listOneRecord)
                    context['team'] = thisTeam
                    context['carts'] = listOfCarts

                context['registry'] = Registry.objects.last()
                return render(request, 'catalog/cart.html', context)


        else:
            thisCat = Categories.objects.get(href=sub)
            context['thisCat'] = thisCat
            thisSub = Subcategories.objects.filter(category_name=thisCat)
            context['subcategories'] = thisSub
            thisToken = str(token)
            thisTeam = Team.objects.get(token=thisToken)
            context['team'] = thisTeam

            context["is_play"] = False if thisTeam.venue == None else True
            if (thisTeam.venue != None):
                context['currentgame'] = True if thisCat.place == thisTeam.venue.place else False
                if 'thisUser' in request.session:
                    context['cookieUserName'] = request.session['thisUser']
                    context['cookieUniqueness'] = request.session['toConnect']

                    try:
                        context['PreviousOrder'] = Archive.objects.filter(team_id = thisTeam.id, place = thisTeam.venue.place,cookie_id = request.session['thisUser'], uniqueness = request.session['toConnect'])
                        context['itemTotal']= context['PreviousOrder'].aggregate(Sum('item_total'))
                    except:
                        pass

                countgoods = 0
                for cart in Carts.objects.filter(team_id=thisTeam.id):
                    countgoods += cart.quantity
                context['countGoods'] = countgoods
                context['dateAndPlace'] = CurrentVenue.objects.first()
            context['registry'] = Registry.objects.last()
            return render(request, 'catalog/subcategory.html', context)

@csrf_exempt
def good(request, token, cat, subcat):
    if (token == "addarchive"):
        archiveCart = Archive.objects.filter(team_id=request.POST['team'], place = thisTeam.venue.place, cookie_id=request.POST['cookiename'],uniqueness=request.POST['cookieuniq'])
        thisTeam = Team.objects.get(id=request.POST['team'])
        for cart in archiveCart:
            try:

                existObj = Carts.objects.get(cookie_id=cart.cookie_id, place = thisTeam.venue.place, team_id=cart.team_id, uniqueness=cart.uniqueness, good = cart.good)
                existObj.quantity += cart.quantity
                existObj.item_total += cart.item_total
                existObj.save()
                thisTeam.cart_total += cart.item_total
            except:
                ibj = Carts.objects.create(good=cart.good, place = thisTeam.venue.place, quantity=cart.quantity, item_total=cart.item_total,team_id=cart.team_id, cookie_id=cart.cookie_id, uniqueness=cart.uniqueness)
                thisTeam.cart_total += cart.item_total
                ibj.save()
        archiveCart.delete()
        thisTeam.save()

        countgoods = 0
        for cart in Carts.objects.filter(team_id=request.POST['team'], place = thisTeam.venue.place):
            countgoods += cart.quantity

        return JsonResponse(toDict(countgoods))
    else:
        context = {}
        sub = str(cat)
        if (sub == "cart"):
            sub = str(subcat)
            if (sub == "delete"):
                thisToken = str(token)
                thisTeam = Team.objects.get(token=thisToken)
                IDgood = int(request.POST['goodId'])
                IDcookie = str(request.POST['cookieId'])
                IDuniq = str(request.POST['uniqueness'])
                thisGood = Goods.objects.get(id=IDgood)
                obj = Carts.objects.get(good=thisGood, place = thisTeam.venue.place, team_id=thisTeam.id, cookie_id = IDcookie, uniqueness=IDuniq)
                cookieTotal = obj.item_total
                deletedID = obj.id
                thisTeam.cart_total -= obj.item_total
                thisTeam.save()
                obj.delete()

                con = dict()
                con.update({0: [deletedID, thisTeam.cart_total, cookieTotal]})

                return JsonResponse(fullDict(con))
            elif (sub == "plus"):
                thisToken = str(token)
                thisTeam = Team.objects.get(token=thisToken)
                IDgood = int(request.POST['goodId'])
                IDcookie = str(request.POST['cookieId'])
                IDuniq = str(request.POST['uniqueness'])
                thisGood = Goods.objects.get(id=IDgood)
                obj = Carts.objects.get(good=thisGood, place = thisTeam.venue.place, team_id=thisTeam.id, cookie_id = IDcookie, uniqueness=IDuniq)
                obj.quantity += 1
                obj.item_total += thisGood.price
                cookieTotal = thisGood.price
                obj.save()
                thisTeam.cart_total += thisGood.price
                thisTeam.save()
                con = dict()
                con.update({0: [obj.id, obj.quantity, thisTeam.cart_total, cookieTotal]})
                return JsonResponse(fullDict(con))
            elif (sub == "minus"):
                thisToken = str(token)
                thisTeam = Team.objects.get(token=thisToken)
                IDgood = int(request.POST['goodId'])
                thisGood = Goods.objects.get(id=IDgood)
                IDcookie = str(request.POST['cookieId'])
                IDuniq = str(request.POST['uniqueness'])
                obj = Carts.objects.get(good=thisGood, place = thisTeam.venue.place, team_id=thisTeam.id, cookie_id=IDcookie, uniqueness=IDuniq)

                obj.quantity -= 1
                obj.item_total -= thisGood.price
                cookieTotal = thisGood.price
                obj.save()
                thisTeam.cart_total -= thisGood.price
                thisTeam.save()

                con = dict()
                con.update({0: [obj.id, obj.quantity, thisTeam.cart_total, cookieTotal]})
                return JsonResponse(fullDict(con))
        else:
            thisCat = Categories.objects.get(href=sub)
            thisToken = str(token)
            thisTeam = Team.objects.get(token=thisToken)
            context['thisCat'] = thisCat
            context["is_play"] = False if thisTeam.venue == None else True
            if (thisTeam.venue != None):
                context['currentgame'] = True if thisCat.place == thisTeam.venue.place else False

                sub = str(subcat)
                thisSubcat = Subcategories.objects.get(href=sub)
                context['thisSub'] = thisSubcat
                thisG = Goods.objects.filter(subcategory_name=thisSubcat)
                context['goods'] = thisG
                if 'thisUser' in request.session:
                    context['cookieUserName'] = request.session['thisUser']
                    context['cookieUniqueness'] = request.session['toConnect']
                    try:
                        context['PreviousOrder'] = Archive.objects.filter(team_id = thisTeam.id, place = thisTeam.venue.place, cookie_id = request.session['thisUser'], uniqueness = request.session['toConnect'])
                        context['itemTotal']= context['PreviousOrder'].aggregate(Sum('item_total'))
                    except:
                        pass
                countgoods = 0
                for cart in Carts.objects.filter(team_id=thisTeam.id):
                    countgoods+=cart.quantity

                context['countGoods'] = countgoods
                context['dateAndPlace'] = CurrentVenue.objects.first()
                context['team'] = thisTeam
            context['registry'] = Registry.objects.last()
            return render(request, 'catalog/good.html', context)


@csrf_exempt
def add_good(request, token, cat, subcat):
    if request.POST:
        if request.is_ajax():
            if 'thisUser' in request.session:
                thisToken = str(token)
                thisTeam = Team.objects.get(token=thisToken)
                IDgood = int(request.POST['good'])
                thisGood = Goods.objects.get(id = IDgood)
                cookie = request.session['thisUser']
                if ("false" == request.session['toConnect']):
                    newuniq = int(Carts.objects.filter(team_id=thisTeam.id, place = thisTeam.venue.place, cookie_id=cookie).latest("uniqueness").uniqueness) + 1
                    obj = Carts.objects.create(good=thisGood, team_id=thisTeam.id, place = thisTeam.venue.place, quantity=1,item_total=thisGood.price, cookie_id=cookie, uniqueness = newuniq)
                    request.session['toConnect'] = str(newuniq)
                else:
                    try:
                        obj = Carts.objects.get(good=thisGood, place = thisTeam.venue.place, team_id=thisTeam.id, cookie_id=cookie, uniqueness = int(request.session['toConnect']))
                        obj.quantity += 1
                        obj.item_total += thisGood.price
                    except:
                        obj = Carts.objects.create(good=thisGood, place = thisTeam.venue.place, team_id=thisTeam.id, quantity=1, item_total=thisGood.price, cookie_id=cookie, uniqueness = int(request.session['toConnect']))

                obj.save()
                thisTeam.cart_total += thisGood.price
                thisTeam.save()
                NumberGood = 0
                for cart in Carts.objects.filter(team_id=thisTeam.id, place = thisTeam.venue.place):
                    NumberGood+=cart.quantity

                return JsonResponse(toDict(NumberGood))
            else:
                return JsonResponse(toDict("N"))

@csrf_exempt
def unconnect_cookie(request):
    if request.POST:
        if request.is_ajax():
            cookie = str(request.POST['NameUnconnectUser']).strip().replace(' ', '_').replace(',','').replace(';','').replace('.','')
            request.session['toConnect'] = "false"
            request.session['thisUser'] = cookie
            return HttpResponse("СПАСИБО!")

@csrf_exempt
def connect_cookie(request, id):
    if request.POST:
        if request.is_ajax():
            cookie = str(request.POST['cookieName']).strip().replace(' ', '_').replace(',','').replace(';','').replace('.','')
            olduniq = int(request.POST['NameConnectUser'])
            request.session['toConnect'] = str(olduniq)
            request.session['thisUser'] = cookie
            return HttpResponse("СПАСИБО!")



def enterWithoutToken(request):
    return render(request, "catalog/WithoutToken.html")




#################
import csv

from django.http import HttpResponse
from django.contrib.auth.models import User

def exportCart(request):

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="cart.csv"'
    response.write(u'\ufeff'.encode('utf8'))

    writer = csv.writer(response, delimiter=';', dialect='excel')
    writer.writerow(['Товар', 'Кол-во', 'Стоимость'])
    event = CurrentVenue.objects.last()
    carts = Carts.objects.filter(place = event.place)
    listCarts = list()
    for c in carts:
        c.team = Team.objects.get(id=c.team_id).name
    i=0
    oldname = carts[0].team
    listNum = 0
    countNum = 0
    while (i < len(carts)):
        if (oldname == carts[i].team):
            j=listNum

            while (j < countNum):
                if (listCarts[j].good == carts[i].good):
                    listCarts[j].quantity += carts[i].quantity
                    listCarts[j].item_total  += carts[i].item_total

                    break
                j+=1
            if (countNum == j):

                countNum +=1
                listCarts.append(carts[i])
        else:
            oldname = carts[i].team
            listNum = len(listCarts)
            countNum = listNum+1
            listCarts.append(carts[i])
        i+=1
    oldname = listCarts[0].team
    writer.writerow(["", "", ""])
    writer.writerow([oldname, "", ""])

    for c in listCarts:
        if (oldname == c.team):
            if (c.good.subcategory_name.prefix):
                writer.writerow([c.good.subcategory_name.prefix + " " + c.good.name, c.quantity, c.item_total])
            else:
                writer.writerow([c.good.subcategory_name.name + " " + c.good.name, c.quantity, c.item_total])
        else:
            oldname = c.team
            writer.writerow(["", "", ""])
            writer.writerow([c.team, "", ""])
            if (c.good.subcategory_name.prefix):
                writer.writerow([c.good.subcategory_name.prefix+" "+c.good.name, c.quantity, c.item_total])
            else:
                writer.writerow([c.good.subcategory_name.name + " " + c.good.name, c.quantity, c.item_total])
    return response

def exportCartURL(request):

    context={}
    temp = list()
    rofel=list()
    i = 0
    event = CurrentVenue.objects.last()
    print(event)
    carts = Carts.objects.filter(place=event.place)

    for c in carts:
        c.team = Team.objects.get(id=c.team_id).name
    oldname = carts[0].team
    listNum = 0
    countNum = 0
    while (i < len(carts)):
        if (oldname == carts[i].team):
            j = listNum
            while (j < countNum):
                if (rofel[j].good == carts[i].good):
                    rofel[j].quantity += carts[i].quantity
                    rofel[j].item_total += carts[i].item_total
                    break
                j += 1
            if (countNum == j):
                countNum += 1

                if (i == 0):
                    carts[i].isNameOfNewTeam = True
                else:
                    carts[i].isNameOfNewTeam = False
                rofel.append(carts[i])
        else:
            oldname = carts[i].team
            listNum = len(rofel)
            countNum = listNum + 1
            carts[i].isNameOfNewTeam = True
            rofel.append(carts[i])
        i += 1

    context['cart']=rofel
    return render(request, "catalog/export_cart.html", context)

def changeexport(request, IdVenue):
    context={}

    temp = list()
    rofel=list()
    i = 0
    hrefEvent = str(IdVenue)
    venue = AllVenues.objects.get(href=hrefEvent)
    event = CurrentVenue.objects.filter(place = venue).last()
    carts = Carts.objects.filter(place=venue)
    context["event"] = event.place.name
    try:
        for c in carts:
            c.team = Team.objects.get(id=c.team_id).name
        oldname = carts[0].team
        listNum = 0
        countNum = 0
        while (i < len(carts)):
            if (oldname == carts[i].team):
                j = listNum
                while (j < countNum):
                    if (rofel[j].good == carts[i].good):
                        rofel[j].quantity += carts[i].quantity
                        rofel[j].item_total += carts[i].item_total
                        break
                    j += 1
                if (countNum == j):
                    countNum += 1

                    if (i == 0):
                        carts[i].isNameOfNewTeam = True
                    else:
                        carts[i].isNameOfNewTeam = False
                    rofel.append(carts[i])
            else:
                oldname = carts[i].team
                listNum = len(rofel)
                countNum = listNum + 1
                carts[i].isNameOfNewTeam = True
                rofel.append(carts[i])
            i += 1

        context['cart']= rofel
    except:
        pass
    return render(request, "catalog/export_cart.html", context)
