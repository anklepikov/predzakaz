from django.conf.urls import url, include

from . import views

urlpatterns = [
    url(r'^export/$', views.exportCartURL),
    url(r'^csv/$',  views.exportCart),
    url(r'^export/(?P<IdVenue>\w+)/$', views.changeexport),
    url(r'^unconnectcookie/$',  views.unconnect_cookie),
    url(r'^connectcookie/(?P<id>\w+)/$',  views.connect_cookie),
    url(r'^(?P<token>\w+)$', views.catalog, name='catalog'),
    url(r'^(?P<token>\w+)/(?P<cat>\w+)$', views.subcategory),
    url(r'^(?P<token>\w+)/(?P<cat>\w+)/(?P<subcat>\w+)$', views.good),
    url(r'^(?P<token>\w+)/(?P<cat>\w+)/(?P<subcat>\w+)/good$', views.add_good),
    url(r'^$', views.enterWithoutToken),
]
