# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from .validators import validate_file_extension, validate_image
# Create your models here.
def toDict(some):
    dict={
        'some': some,
    }
    return dict
def fullDict(con):
    dict={
        'con': con,
    }
    return dict

class AllVenues(models.Model):
    name = models.CharField(default = '', verbose_name = 'Место проведения', blank=False, max_length=100)
    href = models.CharField(max_length=100, verbose_name="Короткое название для ссылки", blank=True, default='')
    def __str__(self):
        return str(self.name)
    class Meta:
        verbose_name_plural = "Заведения"
        verbose_name = "Заведение"

class CurrentVenue(models.Model):
    date = models.DateTimeField(null=True, auto_now=False, blank=True, verbose_name="Дата и время")
    place = models.ForeignKey(AllVenues, on_delete=models.CASCADE, blank=False, null=True, verbose_name="Место")
    def __str__(self):
        return str(self.date)[0:16] + "_" + str(self.place.name)

    class Meta:
        verbose_name_plural = "События"
        verbose_name = "Событие"

class Categories(models.Model):
    name = models.CharField(max_length=100, verbose_name="Название категории", blank=False, default='')
    img = models.ImageField(validators=[validate_image], blank=True, default='', verbose_name="Иконка",upload_to='Categories/')
    href = models.CharField(max_length=100, verbose_name="Ссылка", blank=False, default='')
    has_subcategories = models.BooleanField(default=False, verbose_name="Имеются подкатегории", blank=False)
    place = models.ForeignKey(AllVenues, on_delete=models.CASCADE, blank=True, null=True, verbose_name="Место")

    def __str__(self):
        return self.name
    class Meta:
        verbose_name_plural = "Категории"
        verbose_name = "Категорию"

class Subcategories(models.Model):
    href = models.CharField(max_length=100, verbose_name="Ссылка", blank=False, default='')

    name = models.CharField(max_length=100, verbose_name="Название подкатегории", blank=False, default='')
    img = models.ImageField(validators=[validate_image], blank=True, default='', verbose_name="Иконка",upload_to='Subcategories/')
    category_name = models.ForeignKey(Categories, on_delete=models.CASCADE, blank=False, verbose_name="Название категории")
    prefix = models.CharField(max_length=100, verbose_name="Префикс в корзине", blank=True, default='')
    def __str__(self):
        return self.name
    class Meta:
        verbose_name_plural = "Подкатегории"
        verbose_name = "Подкатегорию"

class Goods(models.Model):
    name = models.CharField(default="", verbose_name="Название товара", blank=False, max_length=100)
    subcategory_name = models.ForeignKey(Subcategories, on_delete=models.CASCADE, blank=False, verbose_name="Название подкатегории")
    href = models.CharField(max_length=100, verbose_name="Ссылка", blank=False, default='')
    img = models.ImageField(validators=[validate_image], default='', verbose_name="Картинка", blank=False,upload_to='Goods/')
    price = models.PositiveIntegerField(default=0, verbose_name="Цена товара", blank=False)
    info = models.TextField(verbose_name="Информация", blank=True, default='')

    def __str__(self):
        return self.name
    class Meta:
        verbose_name_plural = "Товары"
        verbose_name = "Товар"


class Team(models.Model):
    name = models.CharField(default="", verbose_name="Название команды", blank=False, max_length=100)
    token = models.CharField(default="", verbose_name="Уникальный токен команды", blank=True, max_length=20,unique=True)
    venue = models.ForeignKey(CurrentVenue, on_delete=models.SET_NULL, blank=True, null=True, verbose_name="Участвует в событии")
    cart_total = models.PositiveIntegerField(default=0, verbose_name="Общая сумма", blank=False)

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name_plural = "Команды"
        verbose_name = "Команду"



class Carts(models.Model):
    good = models.ForeignKey(Goods, on_delete=models.CASCADE, blank=False, null=True, verbose_name="Товар")
    quantity = models.PositiveIntegerField(default=1, verbose_name="Количество", blank=False)
    item_total = models.PositiveIntegerField(default=0, verbose_name="Общая сумма", blank=False)
    team_id = models.PositiveIntegerField(default=1, verbose_name="Id команды", blank=False)
    cookie_id = models.CharField(default = '', verbose_name = 'Заказал', blank=True,null=True, max_length=60)
    uniqueness = models.PositiveIntegerField(default=1, verbose_name="Уникальность заказывающего", blank=True)
    place = models.ForeignKey(AllVenues, on_delete=models.CASCADE, blank=False, null=True, verbose_name="Заведение")
    def __str__(self):
        team = Team.objects.get(id=self.team_id)
        return str(team.name) + ":  " + str(self.good.name)
    class Meta:
        verbose_name_plural = "Корзина"
        verbose_name = "Корзина"
        ordering = ["team_id"]

class Archive(models.Model):
    good = models.ForeignKey(Goods, on_delete=models.CASCADE, blank=False, null=True, verbose_name="Товар")
    quantity = models.PositiveIntegerField(default=1, verbose_name="Количество", blank=False)
    item_total = models.PositiveIntegerField(default=0, verbose_name="Общая сумма", blank=False)
    team_id = models.PositiveIntegerField(default=1, verbose_name="Id команды", blank=False)
    cookie_id = models.CharField(default = '', verbose_name = 'Заказал', blank=True,null=True, max_length=60)
    uniqueness = models.PositiveIntegerField(default=1, verbose_name="Уникальность заказывающего", blank=True)
    place = models.ForeignKey(AllVenues, on_delete=models.CASCADE, blank=False, null=True, verbose_name="Заведение")

    def __str__(self):
        team = Team.objects.get(id=self.team_id)
        return str(team.name) + ":  " + str(self.good.name)
    class Meta:
        verbose_name_plural = "Архив заказов"
        verbose_name = "Архив заказов"
        ordering = ["team_id"]

class Registry(models.Model):
    href = models.CharField(default = '', verbose_name = 'Ссылка на пост с регистрацией', blank=True,null=True, max_length=130)
    def __str__(self):
        return str("VK")
    class Meta:
        verbose_name_plural = "Ссылка на пост с регистрацией"
        verbose_name = "Ссылку на пост с регистрацией"