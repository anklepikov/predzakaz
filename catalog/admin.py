from django.contrib import admin
from django.utils.crypto import get_random_string
from .models import *



def register_team(modeladmin, request, queryset):
    for elem in queryset:
        if (elem.venue != None):
            ven = elem.venue.place
            elem.venue = None
            elem.cart_total = 0
            elem.save()
            carts = Carts.objects.filter(team_id = elem.id, place = ven)
            try:
                arc = Archive.objects.filter(team_id = elem.id, place = ven)
                arc.delete()
            except:
                pass
            for cart in carts:
                obj = Archive.objects.create(place = cart.place, good=cart.good, team_id=cart.team_id, quantity=cart.quantity, item_total=cart.item_total,cookie_id=cart.cookie_id, uniqueness=cart.uniqueness)
                obj.save()

            carts.delete()
        else:
            elem.save()
register_team.short_description = "Убрать событие"


class teamsAdmin(admin.ModelAdmin):
    model = Team
    list_display = ['name', 'token', 'venue', 'cart_total']
    fields = ['name', 'venue', 'cart_total']
    list_filter = ['venue']
    actions = [register_team]

    def save_model(self, request, obj, form, change):
        if (obj.token==""):
            newtoken=""
            processCantContinue = True
            while (processCantContinue):
                newtoken = get_random_string(length=20)
                queryTeamsWithThisToken = Team.objects.filter(token=newtoken)
                howManyThisToken = len(queryTeamsWithThisToken)
                if (howManyThisToken == 0):
                    processCantContinue = False
            obj.token = newtoken

        obj.save()
    def delete_queryset(self, request, queryset):
        TeamId = queryset.first().id
        setCarts = Carts.objects.filter(team_id = TeamId)
        setCarts.delete()
        queryset.delete()

admin.site.register(Team, teamsAdmin)


class goodsAdmin(admin.ModelAdmin):
    model = Goods
    list_display = ['name', 'subcategory_name', 'price', 'info']
    fields = ['name', 'subcategory_name', 'img', 'price', 'info']
    actions = ['change_hrefs']

    def change_hrefs(self, request, queryset):
        for item in queryset:
            item.href += "_" + item.subcategory_name.category_name.place.href
            item.save()

    change_hrefs.short_description = "Изменить ссылки v2.0"

    def save_model(self, request, obj, form, change):
        obj.href = obj.name.replace(' ', '')
        obj.save()


admin.site.register(Goods, goodsAdmin)

class subcatsAdmin(admin.ModelAdmin):
    model = Subcategories
    list_display = ['name', 'category_name']
    fields = ['name', 'category_name', 'prefix' , 'img']
    actions = ['change_hrefs']
    def change_hrefs(self, request, queryset):
        for item in queryset:
            item.href += "_" + item.category_name.place.href
            item.save()

    change_hrefs.short_description = "Изменить ссылки v2.0"

    def save_model(self, request, obj, form, change):
        obj.href = obj.name.replace(' ', '')
        if (len(Subcategories.objects.filter(category_name=obj.category_name)) == 1 and Subcategories.objects.get(category_name=obj.category_name) != obj):
            cat = obj.category_name
            cat.has_subcategories = True
            cat.save()
        obj.save()
    def delete_queryset(self, request, queryset):
        cat = queryset.first().category_name
        queryset.delete()
        if (len(Subcategories.objects.filter(category_name=cat)) <=1):
            cat.has_subcategories = False
            cat.save()
admin.site.register(Subcategories, subcatsAdmin)



class catsAdmin(admin.ModelAdmin):
    model = Categories
    list_display = ['name', 'has_subcategories', 'place']
    fields = ['name', 'img', 'place']
    actions = ['register_goods', 'change_hrefs']
    list_filter = ['place']
    def register_goods(self, request, queryset):
        firstItem = queryset.first()
        for item in queryset:
            if (item.place != firstItem.place):
                item.place = firstItem.place
                item.save()

    register_goods.short_description = "Изменить заведение по образцу с верхним выбранным"

    def change_hrefs(self, request, queryset):
        for item in queryset:
            item.href += "_" + item.place.href
            item.save()
    change_hrefs.short_description = "Изменить ссылки v2.0"

    def save_model(self, request, obj, form, change):
        setOfSubs = Subcategories.objects.filter(category_name=obj)

        if (len(setOfSubs)>1):
            obj.has_subcategories = True
            obj.href = obj.name.replace(' ', '')
            obj.save()
        elif (len(setOfSubs) == 0):
            obj.has_subcategories = False
            obj.href = obj.name.replace(' ', '')

            obj.save()
            templeSub = Subcategories.objects.create(href=obj.href, name=obj.name, img=obj.img, category_name=obj, prefix = "")
            templeSub.save()
        elif (len(setOfSubs) == 1):
            obj.save()

admin.site.register(Categories, catsAdmin)


admin.site.register(Carts)
admin.site.register(Archive)
admin.site.register(Registry)

admin.site.register(CurrentVenue)
admin.site.register(AllVenues)

