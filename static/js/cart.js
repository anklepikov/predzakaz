$(document).ready(function(){
    $('.delete-elem').submit(function(e){
        e.preventDefault();
            all = $(this).attr('id').split("-");
         $.ajax({
            method : "POST" ,
            url : $(this).attr('action'),
            data: {
                'goodId' : all[0],
                'cookieId' : all[1],
            },

            dataType: 'json',
            success: function(data){
              $(".cart-table").find("."+data.con[0][0]).css("display", "none");
              $(".ItemTotalSum").text(data.con[0][1]);
              console.log(all[1]);
              a = Number($("span."+all[1])[0].innerText);
              a -= Number(data.con[0][2]);
              console.log(a);
              $("span."+all[1])[0].innerText=a;
            }
        })
    });




$('.plus-elem').submit(function(e){
        e.preventDefault();
        aid = $(this).attr('id').split("-");

         $.ajax({
            method : "POST" ,
            url : $(this).attr('action'),
            data: {
                'goodId' : aid[1],
                'cookieId' : aid[2],
            },

            dataType: 'json',
            success: function(data){

              $(".cart-table").find("."+data.con[0][0]).find(".quantity").text(data.con[0][1]);
              $(".ItemTotalSum").text(data.con[0][2]);
              a = Number($("span."+aid[2])[0].innerText);
              a += Number(data.con[0][3]);
              console.log(a);
              $("span."+aid[2])[0].innerText=a;

              }
        })
    });
$('.minus-elem').submit(function(e){
        e.preventDefault();
        aid = $(this).attr('id').split("-");
        quantity = $(this).siblings(".quantity").eq(0).text();
        if (quantity > 1){

           $.ajax({
            method : "POST" ,
            url : $(this).attr('action'),
            data: {
                'goodId' : aid[1],
                'cookieId' : aid[2],
            },

            dataType: 'json',
            success: function(data){

              $(".cart-table").find("."+data.con[0][0]).find(".quantity").text(data.con[0][1]);
              $(".ItemTotalSum").text(data.con[0][2]);
               a = Number($("span."+aid[2])[0].innerText);
              a -= Number(data.con[0][3]);
              console.log(a);
              $("span."+aid[2])[0].innerText=a;
            }
        })
        }

    });
$('.add-good').submit(function(e){
    e.preventDefault();
     $.ajax({
        method : "POST" ,
        url : $(this).attr('action'),
        data: {
            'good' : $(this).children()[1].getAttribute('id'),
        },

        dataType: 'json',
        success: function(data){
           if (data.some == "N"){
                $('#nameModal').modal({
                    show: 'true'
                });
           }
           else{
              $(document).find(".NumberGoods").text(data.some);
           }
        }
     })

});

$('.cookiesave').on("submit", function(e){
    e.preventDefault();
    console.log($('.nameValue')[0].value);
     $.ajax({
        method : "POST" ,
        url : $(this).attr('action'),
        data: {
            'nameUser' : $('.nameValue')[0].value,
        },

        dataType: 'json',
        success: function(data){
            if (data.con[0][0] == "m"){
                for (var i = 1;i<= Number(data.con[0][2]);i++){
                    var table_row = '<form class="connectCookie pb-3" method="post" action="/connectcookie/'+ data.con[i][0] +'/">';
                    table_row +=  '<div class="modal-footer">';
                    table_row += '<input type="submit" value="Совместить с ' + data.con[0][1] + ' - ' + data.con[i][1] + '" class="btn btn-secondary mzgbStyle connectButton" data-dismiss="modal">';
                    table_row += '</div></form>';
                    $('#uniqueContentText').append(table_row);
                }

                var table_row = '<form class="unconnectCookie" method="post" action="/unconnectcookie/">';
                table_row +=  '<div class="modal-footer">';
                table_row += '<input type="submit" value="Сделать отдельный заказ" class="btn btn-secondary mzgbStyle unconnectButton" data-dismiss="modal">';
                table_row += '</div></form>';
                $('#uniqueContentText').append(table_row);
                $('#uniqueModal').modal({
                    show: true
                });
            }
        }
     })
});


$(".saveButton").on('click', function() {
        $(".cookiesave").submit();
});


$("body").on("click" ,".unconnectButton", function(){
    $.ajax({
        method : "POST" ,
        url : "/unconnectcookie/",
        data: {
            'NameUnconnectUser' : $('.nameValue')[0].value,
        },

        dataType: 'json',
        success: function(data){

        }
     })
});

$("body").on("click" ,".connectButton", function(){
console.log($(this).parent().parent().attr('action').split("/")[2]);
    $.ajax({
        method : "POST" ,
        url : $(this).parent().parent().attr('action'),
        data: {
            'NameConnectUser' : $(this).parent().parent().attr('action').split("/")[2],
            'cookieName': $('.nameValue')[0].value,
        },

        dataType: 'json',
        success: function(data){

        }
     })
});

   function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
};
var csrftoken = getCookie('csrftoken');
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
};
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

});

